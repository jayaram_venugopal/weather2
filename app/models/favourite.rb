class Favourite < ApplicationRecord
  
  belongs_to :user
  belongs_to :city
  
  validates_uniqueness_of :city_id, scope: :user_id, on: :create, message: "Already added to favourite"
end
