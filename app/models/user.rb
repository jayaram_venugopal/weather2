class User < ApplicationRecord

  has_many :favourites
  has_many :cities, through: :favourites
  has_many :recent_searches
  has_many :cities, through: :recent_searches

  before_save { self.email = email.downcase }
  has_secure_password

  validates :name,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 6 }


  def update_recent_search(params)
    self.recent_searches.create(params)
  end  
end
