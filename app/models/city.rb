class City < ApplicationRecord
  validates_uniqueness_of :name, on: :create, message: "must be unique"
  validates_presence_of :name, :country_code, :lon, :lat, on: :create

  has_many :favourites
  has_many :users, through: :favourites
  has_many :recent_searches
  has_many :users, through: :recent_searches

end
