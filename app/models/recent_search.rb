class RecentSearch < ApplicationRecord
   
  belongs_to :user
  belongs_to :city

  validates_presence_of :city_id, :user_id, on: :create, message: "can't be blank"
end
