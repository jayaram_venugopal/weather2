class Api::V1::FavouritesController < ApplicationController
  before_action :set_favourite, only: [:destroy]

  def index
    @favourites = current_user.favourites
    render json: @favourites, each_serializer: FavouriteSerializer, status: :ok
  end

  def create
    @favourite = current_user.favourites.create!(favourite_params)    
    json_response(@favourite, :created)
  end

  def destroy
    @favourite.destroy!
    json_response("Record removed!", :ok)
  end
  
  private
  def set_favourite
    @favourite = current_user.favourites.find_by_city_id(params[:id])
    raise ActiveRecord::RecordInvalid if @favourite.nil?
  end
  
  def favourite_params
    params.require(:favourite).permit(:city_id, :user_id)
  end

end
