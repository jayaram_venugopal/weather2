class Api::V1::WeatherController < ApplicationController
  before_action :check_for_params, only: :search
  caches_action :search, cache_path: :search_cache_path, expires_in: 15.minute
  skip_before_action :authorize_request, only: :index

  def index
    
  end
  
  def search
    @weather = get_weather_info(params)
    json_response(@weather, :ok)
  end
  
  private
  def get_weather_info(params)
    weather = Weather::Info.new(params)
    weather_info = weather.get_info
    update_city_detail(weather_info[:city])
    return weather_info
  end

  def update_city_detail(city_data)
    city = City.find_by_name(city_data[:name])
    unless city.present?
      @city = City.create!(city_data) 
      update_recent_search(@city) if @city
    end
  end
  

  def update_recent_search(city)
    params = {:city_id => city.id}
    current_user.update_recent_search(params) 
  end
  

  def check_for_params
    unless params[:city] || (params[:lat] && params[:lon])
      json_response({message: "Search params not found (eg: city or coordinates(lat, lon))"}, :bad_request) 
    end
  end

  protected
  def search_cache_path
    if params[:city]
      "/api/v1/weather/search?city=#{params[:city]}"
    elsif params[:lat] && params[:lon]
      "/api/v1/weather/search?coordinates=#{params[:lat]}, #{params[:lon]}"
    end
  end
end
