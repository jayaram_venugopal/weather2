class Api::V1::RecentSearchesController < ApplicationController
  before_action :set_city_details, only: :city_details

  def index
    @recent = current_user.recent_searches
    render json: @recent, each_serializer: RecentSearchSerializer, status: :ok
  end
end
