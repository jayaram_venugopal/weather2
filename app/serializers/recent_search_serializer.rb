class RecentSearchSerializer < ActiveModel::Serializer
  cache key: 'recent_search'

  has_one :user, serializer: UserSerializer
  has_one :city, serializer: CitySerializer

  attributes :id, :user, :city
end
