class FavouriteSerializer < ActiveModel::Serializer
  cache key: 'favourite'

  has_one :user, serializer: UserSerializer
  has_one :city, serializer: CitySerializer

  attributes :id, :user, :city
end
