class CitySerializer < ActiveModel::Serializer
  attributes :id, :name, :lat, :lon, :country_code
  
  has_many :recent_searches, serializer: RecentSearchSerializer
  has_many :users, through: :recent_searches, serializer: UserSerializer
  
  has_many :favourites, serializer: FavouriteSerializer
  has_many :users, through: :favourites, serializer: UserSerializer
end
