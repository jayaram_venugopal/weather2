class UserSerializer < ActiveModel::Serializer
  has_many :recent_searches, serializer: RecentSearchSerializer
  has_many :cities, through: :recent_searches, serializer: CitySerializer
  
  has_many :favourites, serializer: FavouriteSerializer
  has_many :cities, through: :favourites, serializer: CitySerializer

  attributes :id, :email

end
