Rails.application.routes.draw do
  root 'api/v1/weather#index'
  
  namespace :api do
    namespace :v1 do
      resources :weather, only: [] do
        collection do
          get :search
        end
      end
      
      resources :favourites, only: [:create, :destroy, :index]
      resources :recent_searches, only: [:index]
    end
  end
  
  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'users#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
