module Weather
  class Info
    include NetHttpHelper
    attr_reader :city

    def initialize(data)
      @data = data
    end

    def get_info
      url = URI(weather_detail_api_url)
      url.query = URI.encode_www_form(query_string)
      response = send_get_request(url)
      if response.kind_of?(Net::HTTPSuccess)
        response_json = JSON.parse(response.body)
        forcoast_details = render_response(response_json)
      else
        raise ActiveRecord::RecordNotFound
      end
      return forcoast_details
    end

    private
    def base_url
      ENV['FORCOAST_BASE_URL']
    end

    def weather_detail_api_url
      base_url + "/data/2.5/weather"
    end

    def query_string
      query_params = {}
      query_params["APPID"] = app_id
      if @data[:lat] && @data[:lon] 
        query_params["lat"]= @data[:lat] 
        query_params["lon"]= @data[:lon]
      else
        query_params["q"] = @data[:city] 
      end
      return query_params
    end

    def app_id
      ENV['FORCOAST_KEY']
    end  

    def render_response(params)
      new_params = {
        "city": {
          "name": params["name"],
          "country_code": params["sys"]["country"],
          "lon": params["coord"]["lon"],
          "lat": params["coord"]["lat"]
        },
        "weather_details": {
          "current_temperature": params["main"]["temp"],
          "max_temperature": params["main"]["temp_min"],
          "minimum_temperature": params["main"]["temp_max"],
          "humidity": params["main"]["humidity"],
          "precipitation": precipitation,
          "wind_speed": params["wind"]["speed"],
          "visibility": params["visibility"],
          "overall_weather_status": params["weather"][0]["main"]
        }
      }      
      return new_params
    end

    def precipitation
      rand(0.100)
    end
    
  end
end