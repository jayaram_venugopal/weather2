# -*- encoding : utf-8 -*-
require 'uri'
require 'net/https'

module NetHttpHelper
  def send_get_request(get_request_url)
    get_request_url = URI(get_request_url) if get_request_url.kind_of?(String)
    http = generate_http(get_request_url)
    get_request = generate_get_request(get_request_url)
    
    response = http.request(get_request)
    return response
  end

  def generate_http(uri)
    new_http = Net::HTTP.new(uri.host, uri.port)
    new_http.use_ssl = uri.scheme == "https"
    return new_http
  end

  def generate_get_request(get_url, get_params={})
    new_request = Net::HTTP::Get.new(path_with_query_string(get_url), get_params)
    return new_request
  end

  def path_with_query_string(uri)
    path = uri.path
    path += "?#{uri.query}" if uri.query
    path
  end
end
