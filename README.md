# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version - 2.5.3

* Rails version - 5.2.1.1

* Cache Method - Memcache
  Start: Memcache Server (telnet localhost 11211)

* UserAuthentication - JWT Process

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
