class CreateFavourites < ActiveRecord::Migration[5.2]
  def change
    create_table :favourites do |t|
      t.references :user,           null: false, index: true
      t.references :city,           null: false, index: true
      t.timestamps
    end
  end
end
