class CreateCities < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.string :name
      t.decimal :lat, :precision => 15, :scale => 10, :default => 0.0
      t.decimal :lon, :precision => 15, :scale => 10, :default => 0.0
      t.string :country_code
      t.timestamps
    end
    
    add_index :cities, :name, unique: true
  end
end
